package com.example.duoc.login20.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DUOC on 03-06-2017.
 */

public class ResponseLogin {
    @SerializedName("data")
    private Perfil perfil;

    public ResponseLogin(Perfil perfil) {
        this.perfil = perfil;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }
}
