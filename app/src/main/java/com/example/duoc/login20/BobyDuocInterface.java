package com.example.duoc.login20;

import com.example.duoc.login20.models.ResponseLogin;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by DUOC on 03-06-2017.
 */

public interface BobyDuocInterface {
    @GET("loginAlumno")
    Call<ResponseLogin> login(@Query("usuario") String user,
                              @Query("password") String pass);
}
